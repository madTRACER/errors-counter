//============================================================================
// Name        : my_prj.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

class Measurements{
private:
	double sc = 2.78; //student coefficient
	double sd; //std deviation value
	double ae; //absolute err value
	double re; //relative err value
public:
	double std_dev(double m1,double m2,double m3,double m4,double m5){
		double sq_sum = (m1*m1)+(m2*m2)+(m3*m3)+(m4*m4)+(m5*m5);
		sd = sqrt(sq_sum/20);
		return sd;
	}
	double abs_err(){
		ae = sd*sc;
		return ae;
	}
	double rel_err(double am){
		re = ae/am;
		return re;
	}
};

Measurements m;

int main() {
	double res_sd;
	double res_ae;
	double res_re;
        res_sd = m.std_dev(0.56,0.56,-1.44,0.76,-0.44);
	cout << "Std dev is: " << res_sd << endl;
        res_ae = m.abs_err();
	cout << "Absolute err is: " << res_ae << endl;
        res_re = m.rel_err(66.56);
	cout << "Relative err is: " << (res_re * 100) << " %" << endl;
        cout << "Final result is:" << res_sd << " +- " << res_re << endl;
	return 0;
}
